#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
//#include <bits/stdc++.h>
#include <cstring>
#include <string.h>
#include <string>
#include <algorithm>
#include <set>
#include <utility>
#include <vector>
#include <iostream>
using namespace std;
typedef long long int ll;
typedef long double dl;


typedef pair<dl,dl> pdi;
typedef pair<ll,ll> pii;
typedef pair<ll,pii> piii;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
//cout<<fixed;
//cout.precision(12);


#define LIBNET_LIL_ENDIAN 1
typedef struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
    uint8_t ip_hl:4,      /* header length */
           ip_v:4;         /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
    uint8_t ip_v:4,       /* version */
           ip_hl:4;        /* header length */
#endif
    u_int8_t ip_tos;       /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY      0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT    0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY   0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST       0x02
#endif
    u_int16_t ip_len;         /* total length */
    u_int16_t ip_id;          /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000        /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000        /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000        /* more fragments flag */
#endif 
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;          /* time to live */
    u_int8_t ip_p;            /* protocol */
    u_int16_t ip_sum;         /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
}libnet_ipv4_hdr;

typedef struct libnet_tcp_hdr
{
    u_int16_t th_sport;       /* source port */
    u_int16_t th_dport;       /* destination port */
    u_int32_t th_seq;          /* sequence number */
    u_int32_t th_ack;          /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
    u_int8_t th_x2:4,         /* (unused) */
           th_off:4;        /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t th_off:4,        /* data offset */
           th_x2:4;         /* (unused) */
#endif
    u_int8_t  th_flags;       /* control flags */
#ifndef TH_FIN
#define TH_FIN    0x01      /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN    0x02      /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST    0x04      /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH   0x08      /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK    0x10      /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG    0x20      /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE    0x40
#endif
#ifndef TH_CWR
#define TH_CWR    0x80
#endif
    u_int16_t th_win;         /* window */
    u_int16_t th_sum;         /* checksum */
    u_int16_t th_urp;         /* urgent pointer */
}libnet_tcp_hdr;

string hst;
void usage() {
	printf("syntax : netfilter-test <host>\nsample : netfilter-test test.gilgil.net");
}

/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi;
	int ret;
	unsigned char *data;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
		printf("hw_protocol=0x%04x hook=%u id=%u ",
			ntohs(ph->hw_protocol), ph->hook, id);
	}

	hwph = nfq_get_packet_hw(tb);
	if (hwph) {
		int i, hlen = ntohs(hwph->hw_addrlen);

		printf("hw_src_addr=");
		for (i = 0; i < hlen-1; i++)
			printf("%02x:", hwph->hw_addr[i]);
		printf("%02x ", hwph->hw_addr[hlen-1]);
	}

	mark = nfq_get_nfmark(tb);
	if (mark)
		printf("mark=%u ", mark);

	ifi = nfq_get_indev(tb);
	if (ifi)
		printf("indev=%u ", ifi);

	ifi = nfq_get_outdev(tb);
	if (ifi)
		printf("outdev=%u ", ifi);
	ifi = nfq_get_physindev(tb);
	if (ifi)
		printf("physindev=%u ", ifi);

	ifi = nfq_get_physoutdev(tb);
	if (ifi)
		printf("physoutdev=%u ", ifi);

	ret = nfq_get_payload(tb, &data);
	if (ret >= 0)
		printf("payload_len=%d\n", ret);

	fputc('\n', stdout);

	return id;
}
set<pii> hss;
int p1 = 10000019; //could be change or get bigger with using long long int 
int p2 = 30000001;

pii hash_string(string str) {
    int ret1 = 1;
    int ret2 = 1;

    int c;
    for(int i=0;i<str.size();i++){
    	c=str[i];
        ret1 = (ret1*33 + c)%p1; /* hash * 33 + c */
        ret2 = (ret2*33 + c)%p2; /* hash * 33 + c */
    }
    return make_pair(ret1,ret2);
}

void table_parsing(char *filename)
{
	int cnt=0;
	FILE* fp = fopen(filename, "r");
	while(!feof(fp)){
		cnt++;
		char buf[1024]="";
		fgets(buf, 1024, fp);

		if(strlen(buf)==0) break;

		char *p = strtok(buf, ",");
		p = strtok(NULL, "\n");
		string newp=string(p);
		newp.pop_back();
		pii x=hash_string(newp);
		hss.insert(x);
/*
		if(cnt<10){
			cout<<"sample blocked "<<newp<<'\n';
			if(newp!="google.com"){
				cout<<"sample blocked "<<newp<<' '<<newp.size()<<'\n';
			}
			cout<<x.ff<<' '<<x.ss<<'\n';
		}
*/		

    }
	fclose(fp);
}

string lists[9] = {"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"};

bool chking(string s){
	for(ll i=0;i<9;i++){
		if( s.substr(0,lists[i].size()) ==lists[i]){
			return true;
		}
	}

	return false;
}

bool chking2(string s){

	s = s.substr(s.find("Host: ")+6);
	s = s.substr(0, s.find("\r\n"));
				 
	pii x = hash_string(s);
	if(hss.find(x)!=hss.end()) return true;
	return false;
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data) //여기에 페킷 처리하는 함수 넣기
{

	cout<<'\n'<<"//////////// doing cb /////////////"<<'\n';
	u_int32_t id = print_pkt(nfa);
	u_char *pkt_hdr;
	int len = nfq_get_payload(nfa, &pkt_hdr);
	

    struct libnet_ipv4_hdr *ip_hdr = (struct libnet_ipv4_hdr*)(pkt_hdr);
    int bas_len=0;

    if(ip_hdr->ip_p == 6){
    	struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)(pkt_hdr+ip_hdr->ip_hl * 4);

		if(ntohs(tcp_hdr->th_sport)==80||ntohs(tcp_hdr->th_dport)==80){
			string http_data = (char *)(pkt_hdr+ip_hdr->ip_hl *4+ tcp_hdr->th_off  * 4);
			if( (chking(http_data)) && (chking2(http_data)) ){
				cout<<http_data<<'\n';
				printf("to be blocked\n");
				return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
			}
			else{
				cout<<http_data<<'\n';
				printf("not to be blocked\n");
			}
		}	
		else{
			printf("it's not http\n");
		}
    }
    else {
    	printf("it's not tcp\n");
    }


    //cout<<string(ip_hdr)<<'\n';
	//printf("entering callback\n");
	return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));
	/*
	table_parsing(argv[1]);
	string s="gilgil.net";
	pii test = hash_string(s);

	if(hss.find(test)!=hss.end()){
		cout<<"mang"<<'\n';
	}
	else{
		cout<<"test_clear"<<'\n';
	}

	string s2="google.com";
	pii test2 = hash_string(s2);
	cout<<"test2 "<<test2.ff<<' '<<test2.ss<<'\n';
	if(hss.find(test2)==hss.end()){
		cout<<"mang"<<'\n';
	}
	else{
		cout<<"test_clear"<<'\n';
	}

	string s3="google.com";
	pii test3 = hash_string(s3);
	cout<<"test3 "<<test3.ff<<' '<<test3.ss<<'\n';
	if(hss.find(test3)==hss.end()){
		cout<<"mang"<<'\n';
	}
	else{
		cout<<"test_clear"<<'\n';
	}
*/

	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. nfq_nlmsg_verdict_putPlease, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);

	exit(0);
}

